
// Rewrite a given date via strptime/strftime.

#define _XOPEN_SOURCE
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

static void
usage(FILE *out)
{
  fprintf(out, "Usage: rewrite-time DATE FORMAT_IN FORMAT_OUT\n");
}

static void misuse() { usage(stderr); exit(2); }

int
main(int argc, char **argv)
{
  if (argc != 4) misuse();
  const char * const date = argv[1];
  const char * const fmt_in = argv[2];
  const char * const fmt_out = argv[3];

  struct tm tm;
  char *next = strptime(date, fmt_in, &tm);
  if (!next) {
    fprintf(stderr, "\"%s\" doesn't match format \"%s\"\n", date, fmt_in);
    exit(2);
  }

  char out[1024];
  const size_t n = strftime(out, 1024, fmt_out, &tm);
  if (!n) {
    fprintf(stderr, "Rewriting to \"%s\" produced nothing\n", fmt_out);
    exit(2);
  }
  puts(out);

  return 0;
}
